#!/bin/sh

if [ -z $DB_URL ];then
  echo DB_URL is not set
  exit
fi

echo Starting migrations
echo $DB_URL

/migrate -path /migrations -database $DB_URL up

