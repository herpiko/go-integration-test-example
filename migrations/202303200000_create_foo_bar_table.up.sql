DO $$ BEGIN
  CREATE EXTENSION pgcrypto;
EXCEPTION
  WHEN duplicate_object THEN null;
END $$;


CREATE TABLE foo (
  id UUID DEFAULT gen_random_uuid(),
  bar TEXT NOT NULL,
  created_at TIMESTAMP DEFAULT NOW());
