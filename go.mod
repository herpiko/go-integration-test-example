module foo

go 1.20

require github.com/joho/godotenv v1.3.0

require (
	github.com/go-chi/chi v1.5.4 // indirect
	github.com/go-chi/chi/v5 v5.0.8 // indirect
	github.com/go-chi/cors v1.2.1 // indirect
	github.com/go-playground/assert/v2 v2.2.0 // indirect
	github.com/golang-migrate/migrate/v4 v4.15.2 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/lib/pq v1.10.7 // indirect
	github.com/mattes/migrate v3.0.1+incompatible // indirect
	go.uber.org/atomic v1.7.0 // indirect
)

replace cdl_blob => ./external/cdl_blob
