package main

import (
	"database/sql"
	"fmt"
	"foo/internal/controller"
	"foo/internal/route"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	_ = godotenv.Load()
	log.Println("foo")

	host := os.Getenv("DB_HOST")
	port, _ := strconv.Atoi(os.Getenv("DB_PORT"))
	username := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASS")
	dbname := os.Getenv("DB_NAME")
	log.Println(password)
	args := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, username, password, dbname)
	db, err := sql.Open("postgres", args)
	if err != nil {
		panic(err.Error())
	}
	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	c := controller.Controller{
		DB: db,
	}

	router := chi.NewRouter()

	route.MountRouter(router, &c)
	err = http.ListenAndServe(":8000", router)
	if err != nil {
		panic(err.Error())
	}
}
