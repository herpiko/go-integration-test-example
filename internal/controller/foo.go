package controller

import (
	"context"
	"database/sql"
	"encoding/json"
	"foo/internal/repository"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func (c *Controller) GetFoo(w http.ResponseWriter, r *http.Request) {
	ID := chi.URLParam(r, "id")
	fooRepo := repository.FooRepository{DB: c.DB}
	foo, err := fooRepo.GetFoo(context.Background(), ID)
	if err != nil && err != sql.ErrNoRows {
		w.WriteHeader(500)
		return
	}
	jsonResp, err := json.Marshal(foo)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	w.Write(jsonResp)

	w.WriteHeader(200)
}
