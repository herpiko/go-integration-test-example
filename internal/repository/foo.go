package repository

import (
	"context"
	"database/sql"
	"time"
)

type FooRepository struct {
	DB *sql.DB
	Tx *sql.Tx
}

type Foo struct {
	ID        string     `json:"id"`
	Bar       string     `json:"bar"`
	CreatedAt *time.Time `json:"created_at"`
}

func (r *FooRepository) GetFoo(ctx context.Context, ID string) (Foo, error) {
	var foo Foo
	statement := `SELECT id, bar, created_at FROM foo WHERE id = $1`
	row := r.DB.QueryRowContext(ctx, statement, ID)
	err := row.Scan(&foo.ID, &foo.Bar, &foo.CreatedAt)

	return foo, err
}
