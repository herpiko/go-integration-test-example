package route

import (
	"foo/internal/controller"

	"github.com/go-chi/chi/v5"
)

func FooRoute(r chi.Router, c *controller.Controller) {
	r.Route("/", func(r chi.Router) {
		r.Get("/foo", c.GetFoo)
	})
}
