package route

import (
	"foo/internal/controller"

	"github.com/go-chi/chi/v5"
	chimiddleware "github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
)

func MountRouter(r *chi.Mux, c *controller.Controller) {
	r.Use(chimiddleware.Logger)

	r.Group(func(r chi.Router) {
		// CORS
		r.Use(cors.Handler(corsOptions()))

		r.Group(func(r chi.Router) {
			FooRoute(r, c)
		})
	})
}

func corsOptions() cors.Options {
	return cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token", "X-API-KEY"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}
}
