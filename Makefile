COMPOSE=deployments/docker/docker-compose.yaml
NAMESPACE=dev${CI_PIPELINE_ID}
MIGRATION_PATH=`pwd`/migrations
BUILDER_IMAGE=golang:1.20-alpine

include .env
export $(shell sed 's/=.*//' .env)
UID=$(shell whoami)

# Cache
ifeq ($(CACHE_PREFIX),)
	CACHE_PREFIX=/tmp
endif

# Pipeline related
TI = 
ifeq ($(CI_PIPELINE_ID),)
	TI = -ti
endif
DIND_PREFIX ?= $(HOME)
ifneq ($(HOST_PATH),)
	DIND_PREFIX := $(HOST_PATH)
endif
PREFIX=$(shell echo $(PWD) | sed -e s:$(HOME):$(DIND_PREFIX):)

.PHONY : test


check-sql-seq:
	./scripts/check-sql-seq.sh

prep:
	docker kill $$(docker ps | grep postgres | grep dev | grep testdb | cut -d ' ' -f 1) || true
	docker rm $$(docker ps | grep postgres | grep dev | grep testdb | cut -d ' ' -f 1) || true
	docker network create -d bridge ${NAMESPACE} ; true 
	docker-compose -f ${COMPOSE} -p ${NAMESPACE} up -d --force-recreate testdb
	docker run --network ${NAMESPACE} willwill/wait-for-it db:5432 -- echo "database is up"

migrate: build_migrate
	docker-compose -f ${COMPOSE} -p ${NAMESPACE} run testdb dropdb -h ${DB_HOST} -U ${DB_USER} -w ${DB_NAME} || true
	docker-compose -f ${COMPOSE} -p ${NAMESPACE} run testdb createdb -h ${DB_HOST} -U ${DB_USER} -w ${DB_NAME} || true
	docker-compose -f ${COMPOSE} -p ${NAMESPACE} up --force-recreate migrate

build_migrate: 
	docker-compose -f ${COMPOSE} -p ${NAMESPACE} build --no-cache migrate 


dbshell:
	docker exec -ti $$(docker ps | grep postgres | grep dev | grep testdb | cut -d ' ' -f 1) psql -U db -d testdb

clean:
	docker-compose -f ${COMPOSE} -p ${NAMESPACE} stop 
	docker-compose -f ${COMPOSE} -p ${NAMESPACE} rm -f testdb
	docker network remove ${NAMESPACE} ; true 
	docker volume prune -f


test:
	source .env && go clean -testcache && DB_HOST=localhost MIGRATION_PATH=../migrations go test -v ./tests
