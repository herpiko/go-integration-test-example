package test

import (
	"database/sql"
	fmt "fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"testing"

	migrate "github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/joho/godotenv"
	_ "github.com/mattes/migrate/source/file"
)

func TestMain(m *testing.M) {
	log.SetFlags(log.Lshortfile | log.Lmicroseconds)
	var err error

	// Prepare migration
	_ = godotenv.Load("../.env")
	host := os.Getenv("DB_HOST")
	port, _ := strconv.Atoi(os.Getenv("DB_PORT"))
	username := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASS")
	dbName := os.Getenv("DB_NAME")
	log.Println(os.Getenv("MIGRATION_PATH"))

	migrationPath := "file://" + os.Getenv("MIGRATION_PATH")
	if migrationPath == "file://" {
		log.Println("MIGRATION_PATH environment is not set, using default value")
		cwd, _ := filepath.Abs(filepath.Dir(os.Args[0]))
		migrationPath = "file://" + cwd + "/migrations"
	}
	args := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, username, password, dbName)
	migrateArgs := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", username, password, host, strconv.Itoa(port), dbName)
	log.Println(fmt.Sprintf("Migrating %s from %s", migrateArgs, migrationPath))
	log.Println(migrationPath)

	migration, err := migrate.New(
		migrationPath,
		migrateArgs)
	if err != nil {
		log.Fatal(err)
	}
	err = migration.Drop()
	if err != nil && err.Error() != "no change" {
		log.Fatal(err)
	}
	_, _ = migration.Close()
	migration, err = migrate.New(
		migrationPath,
		migrateArgs)
	if err != nil {
		log.Fatal(err)
	}
	err = migration.Up()
	if err != nil && err.Error() != "no change" {
		log.Fatal(err)
	}
	_, _ = migration.Close()

	// Backup migrated db
	db, err := sql.Open("postgres", args)
	if err != nil {
		panic(err.Error())
	}
	_, _ = db.Exec(`
	DROP DATABASE ` + dbName + `_ready`)
	_, err = db.Exec(`
	CREATE DATABASE ` + dbName + `_ready WITH TEMPLATE ` + dbName)
	if err != nil {
		log.Fatal(err)
	}
	db.Close()

	code := m.Run()

	log.Println("exiting")
	os.Exit(code)
}
