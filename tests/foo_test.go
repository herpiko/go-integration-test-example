package test

import (
	"context"
	"database/sql"
	"foo/internal/repository"
	"testing"

	"github.com/go-playground/assert/v2"
	_ "github.com/lib/pq"
)

func TestFoo(t *testing.T) {
	this, err := BaseTest(true)
	defer this.DB.Close()
	assert.Equal(t, nil, err)

	var ID string
	err = this.DB.QueryRow("INSERT INTO foo (bar) VALUES ('yo') RETURNING id").Scan(&ID)
	assert.Equal(t, nil, err)

	fooRepo := repository.FooRepository{DB: this.DB}
	foo, err := fooRepo.GetFoo(context.Background(), ID)
	assert.Equal(t, nil, err)
	assert.Equal(t, foo.ID, ID)
}

func TestFooButEmpty(t *testing.T) {
	this, err := BaseTest(true)
	defer this.DB.Close()
	assert.Equal(t, nil, err)

	var ID string
	ID = "623537bd-aa50-4ef7-9b98-ccada1b4597b"
	fooRepo := repository.FooRepository{DB: this.DB}
	_, err = fooRepo.GetFoo(context.Background(), ID)
	assert.Equal(t, sql.ErrNoRows, err)
}
