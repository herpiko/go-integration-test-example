package test

import (
	"database/sql"
	"fmt"
	controller "foo/internal/controller"
	"log"
	"os"
	"strconv"

	_ "github.com/lib/pq"
)

var baseTest *controller.Controller

func BaseTest(isDrop bool) (*controller.Controller, error) {
	if baseTest != nil {
		baseTest.DB.Close()
	}
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	host := os.Getenv("DB_HOST")
	port, _ := strconv.Atoi(os.Getenv("DB_PORT"))
	username := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASS")
	dbName := os.Getenv("DB_NAME")
	args := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, username, password, dbName)

	if isDrop {
		db, err := sql.Open("postgres", args)
		if err != nil {
			panic(err.Error())
		}
		// Ignore err, we just want to switch the db
		_, _ = db.Exec("CREATE DATABASE " + dbName + "_tmp")
		db.Close()

		args := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, username, password, dbName+"_tmp")
		db, err = sql.Open("postgres", args)
		if err != nil {
			panic(err.Error())
		}

		_, err = db.Exec("DROP DATABASE " + dbName)
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}
		_, err = db.Exec("CREATE DATABASE " + dbName + " WITH TEMPLATE " + dbName + "_ready")
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}
		db.Close()
	}

	db, err := sql.Open("postgres", args)
	if err != nil {
		panic(err.Error())
	}

	baseTest = &controller.Controller{DB: db}

	return baseTest, nil
}
