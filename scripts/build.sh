#!/bin/sh

SRC=$1
cd /src
env
go vet
CGO_ENABLED=0 GOOS=linux go build -o /build/main ./cmd/$SRC/main.go
