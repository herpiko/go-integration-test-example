#!/bin/sh

apk add --update curl git gcc musl-dev 
su -l nobody
cd /src/

export MIGRATION_PATH=/migrations
go test --failfast -v -timeout 0 -test.parallel 4 -cover -coverprofile=coverage.out $@
