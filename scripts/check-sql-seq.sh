#!/bin/bash

cd migrations/deploy

files=()
duplicates=()

for f in *; do
    seq="${f%%_*}"
    for i in "${files[@]}"; do
        next_seq="${i%%_*}"
        if [[ "${next_seq}" == "${seq}" ]]; then
            if [[ "$i" == *"down"* ]]; then
              continue
            fi
            duplicates+=("$f = $i")
        fi
    done
    files+=($f)
done

duplicate=false
for fd in "${duplicates[@]}"; do
    echo "${fd}"
    duplicate=true
done

if [[ "${duplicate}" == true ]]; then
        echo "Found duplicate sequence. exiting..."
        exit 1
    else
        echo "No duplicate sequence found"
fi
