# go-integration-test-example

- `make prep` to spin up the postgres database.
- `make migrate` to migrate the SQL migration files.
- At this point you can `make dbshell` to enter the psql shell and inspect the migrated database.
- `make test` to run the test.
